.TH agar\-math\-config 1 "September 16, 2011" "" "agar"

.SH NAME
agar\-math\-config \- get package configuration for libag\-math 

.SH SYNOPSIS
.B agar\-math\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-math
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-math.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar math library for
application development.
