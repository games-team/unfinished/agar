.TH agar\-core\-config 1 "September 16, 2011" "" "agar"

.SH NAME
agar\-core\-config \- get package configuration for libag\-core 

.SH SYNOPSIS
.B agar\-core\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-core
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-core.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar core library for
application development.
