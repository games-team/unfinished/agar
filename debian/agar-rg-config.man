.TH agar\-rg\-config 1 "September 16, 2011" "" "agar"

.SH NAME
agar\-rg\-config \- get package configuration for libag\-rg 

.SH SYNOPSIS
.B agar\-rg\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-rg
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-rg.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar rg library for
application development.
